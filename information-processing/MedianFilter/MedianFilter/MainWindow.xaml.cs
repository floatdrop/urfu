﻿using System;
using System.ComponentModel;
using System.Windows;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.WPF;

namespace MedianFilter
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {

        private Image<Rgb, Double> _currentImage;
        private string _filename;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void OpenPictureFileClick(object sender, RoutedEventArgs e)
        {
            var dlg = new Microsoft.Win32.OpenFileDialog
                {Filter = "Pictures |*.bmp;*.png;*.jpg"};
            var result = dlg.ShowDialog();
            if (result == true)
            {
                _filename = dlg.FileName;
                _currentImage = new Image<Rgb, double>(_filename);
                ProcessImage.IsEnabled = true;
                OriginalImage.Source = BitmapSourceConvert.ToBitmapSource(_currentImage);
            }
        }

        private void ProcessImageClick(object sender, RoutedEventArgs e)
        {
            FreezeUi(true);
            var newImage = new Image<Rgb, double>(_currentImage.Width, _currentImage.Height);

            var worker = new BackgroundWorker {WorkerReportsProgress = true};
            if (UseEmguCv.IsChecked.HasValue && UseEmguCv.IsChecked.Value)
                worker.DoWork += (o, args) =>  ProcessImageWithEmguCV(args, worker, out newImage);
            else
                worker.DoWork += (o, args) => ProcessImageWithMedianFiler(args, worker, newImage);
            
            worker.ProgressChanged += delegate(object o, ProgressChangedEventArgs args) { WorkingLabel.Content = String.Format("Работаю: {0}%", args.ProgressPercentage); };
            worker.RunWorkerCompleted += delegate
                                             {
                                                 ProcessedImage.Source = BitmapSourceConvert.ToBitmapSource(newImage);
                                                 FreezeUi(false);
                                             };
            worker.RunWorkerAsync((int) WindowSize.Value);
        }

        private void ProcessImageWithEmguCV(DoWorkEventArgs args, BackgroundWorker worker, out Image<Rgb, double> newImage)
        {
            // Because Median is not working is not 
            newImage = _currentImage.SmoothBlur((int)args.Argument, (int)args.Argument);
        }

        private void FreezeUi(bool freeze)
        {
            UseEmguCv.Visibility = freeze ? Visibility.Hidden : Visibility.Visible;
            OpenPictureFile.IsEnabled = !freeze;
            ProcessImage.IsEnabled = !freeze;
            WindowSize.IsEnabled = !freeze;
            WorkingLabel.Visibility = freeze ? Visibility.Visible : Visibility.Hidden;
        }

        private void ProcessImageWithMedianFiler(DoWorkEventArgs args, BackgroundWorker worker, Image<Rgb, double> newImage)
        {
            var windowSize = (int) args.Argument;
            for (var x = windowSize; x < _currentImage.Width - windowSize; x++)
            {
                worker.ReportProgress((int) ((double) (x - windowSize)/(_currentImage.Width - 2*windowSize)*100.0));
                for (var y = windowSize; y < _currentImage.Height - windowSize; y++)
                    newImage[y, x] = GetMedian(x, y, windowSize);
            }
        }

        private Rgb GetMedian(int x, int y, int windowSize)
        {
            var len = ((windowSize + 1) * (windowSize + 1));
            var r = new double[len];
            var g = new double[len];
            var b = new double[len];

            var i = 0;

            for (var fx = 0; fx < windowSize; fx++)
                for (var fy = 0; fy < windowSize; fy++)
                {
                    r[i] = _currentImage[y + fy - windowSize, x + fx - windowSize].Red;
                    g[i] = _currentImage[y + fy - windowSize, x + fx - windowSize].Green;
                    b[i] = _currentImage[y + fy - windowSize, x + fx - windowSize].Blue;
                    i++;
                }

            return new Rgb(Median(r), Median(g), Median(b));

        }

        private double Median(double[]p0)
        {
            Array.Sort(p0);
            var middle = p0[p0.Length / 2];
            return middle;
        }
    }
}

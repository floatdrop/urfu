﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.WPF;

namespace VideoBlur
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string _filename;
        private Image<Bgr, double> _currentImage;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void OpenImageClick(object sender, RoutedEventArgs e)
        {
            var dlg = new Microsoft.Win32.OpenFileDialog { Filter = "Pictures |*.bmp;*.png;*.jpg" };
            var result = dlg.ShowDialog();
            if (result == true)
            {
                _filename = dlg.FileName;
                _currentImage = new Image<Bgr, double>(_filename);
                OriginalImage.Source = BitmapSourceConvert.ToBitmapSource(_currentImage);
            }
        }

        private void CreateVideoClick(object sender, RoutedEventArgs e)
        {
            using (var videoWriter = new VideoWriter("video.avi", 20, _currentImage.Width, _currentImage.Height, true))
            {
                try
                {
                    Directory.CreateDirectory("videoFrames");
                    for (int i = 1; i <= 100; i++)
                    {
                        var blurred = _currentImage.SmoothBlur(i, i);
                        blurred.Save(@"videoFrames\img" + i + ".png");
                        videoWriter.WriteFrame(blurred.Convert<Bgr, byte>());
                    }
                }
                catch (Exception eeeee)
                {
                    MessageBox.Show(eeeee.ToString());
                }
            }
        }
    }
}

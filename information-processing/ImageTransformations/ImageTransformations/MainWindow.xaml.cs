﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.WPF;

namespace ImageTransformations
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string _filename;
        private Image<Rgb, byte> _currentImage;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void OpenImageFile_Click(object sender, RoutedEventArgs e)
        {
            var dlg = new Microsoft.Win32.OpenFileDialog { Filter = "Pictures |*.bmp;*.png;*.jpg" };
            var result = dlg.ShowDialog();
            if (result == true)
            {
                _filename = dlg.FileName;
                _currentImage = new Image<Rgb, byte>(_filename);
                ProcessImage.IsEnabled = true;
                OriginalImage.Source = BitmapSourceConvert.ToBitmapSource(_currentImage);
            }
        }



		private void ProcessImage_Click(object sender, RoutedEventArgs e)
		{
			var sourceCorner = new PointF[4]
			{
				new PointF(0,0), 
				new PointF(_currentImage.Width,0), 
				new PointF(_currentImage.Width,_currentImage.Height), 
				new PointF(0,_currentImage.Height)
			};
			var destCorner = new PointF[4]
			{
				GetPoint(APoint),
				GetPoint(DPoint),
				GetPoint(CPoint),
				GetPoint(BPoint),
			};
			HomographyMatrix homography = CameraCalibration.GetPerspectiveTransform(sourceCorner, destCorner);
			HomographyMatrix homographyReverse = CameraCalibration.GetPerspectiveTransform(destCorner, sourceCorner);
			try
			{
				SecondImage.Source =
					BitmapSourceConvert.ToBitmapSource(_currentImage.WarpPerspective(homography, INTER.CV_INTER_CUBIC,
																					 WARP.CV_WARP_INVERSE_MAP, new Rgb(1, 1, 1)));
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
			try
			{
				FirstImage.Source =
					BitmapSourceConvert.ToBitmapSource(_currentImage.WarpPerspective(homographyReverse, INTER.CV_INTER_CUBIC,
																					 WARP.CV_WARP_INVERSE_MAP, new Rgb(1, 1, 1)));
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
		}

	    private PointF GetPoint(Grid aPoint)
	    {
			return new PointF(
				(float)((double)aPoint.GetValue(Canvas.TopProperty) * ((float)_currentImage.Width) / OriginalImage.ActualWidth),
				(float)((double)aPoint.GetValue(Canvas.LeftProperty) * ((float) _currentImage.Height) / OriginalImage.ActualHeight)
			);
	    }
    }
}

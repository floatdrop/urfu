#!/usr/bin/python
import os, unittest
import task

class TestFilesInDirectory(unittest.TestCase):

    def setUp(self):
        files = os.listdir(".")
        self.testfiles = [x for x in files if x.startswith("input-")]
        self.testfiles.sort()

    def testFiles(self):
        print "Running solve for all files in current directory..."
        for test in self.testfiles:
            result = task.solve(test)
            if test.endswith(".ok"):
                print "File %s\t: %r" % (test, result)
                self.assertTrue(result)
            if test.endswith(".err"):
                print "File %s\t: %r" % (test, not result)
                self.assertFalse(result)

if __name__ == '__main__':
    unittest.main()

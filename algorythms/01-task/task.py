#!/usr/bin/python
import sys, os, operator
import math

def solve(filePath):
    f = open(filePath, 'r')
    lines = f.readlines()
    T = [float(l) for l in lines[0].strip().split()]
    E = [float(l) for l in lines[1].strip().split()]
    X = [float(l) for l in lines[2].strip().split()]
    X.sort()
    R = [a + b for a, b in zip(T, E)]
    TE = zip(T, E)
    TER = zip(TE, R)
    TER.sort(key=operator.itemgetter(1))
    for i in range(0, len(TER)):
        if math.fabs(TER[i][0][0] - X[i]) > TER[i][1]:
            return False
    return True

if __name__ == '__main__':
    if len(sys.argv) < 2:
        sys.exit('Usage: %s input-file' % sys.argv[0])
    if not os.path.exists(sys.argv[1]):
        sys.exit('ERROR: File %s does not exists!' % sys.argv[1])
    print solve(sys.argv[1])

#!/usr/bin/python

#p = [
#    [ 1.0 , 1.1 , 0.1 ],
#    [ 0.1 , 1.0 , 2.1 ],
#    [ 3.1 , 0.1 , 1.0 ]
#]

p = [
    [ 1, 0.5, 2 ],
    [ 2, 1, 0.25 ],
    [ 0.5, 4, 1 ]
]

n = [[-1,-1,-1],[-1,-1,-1],[-1,-1,-1]]

for i in range(0,3):
    for j in range(0,3):
        n[i][j] = i

for k in range(0,3):
    for j in range(0,3):
        for i in range(0,3):
            if p[i][k] * p[k][j] > p[i][j]:
                p[i][j] = p[i][k] * p[k][j]
                n[i][j] = n[k][j]

def path(i,j):
    k = n[i][j]
    if k == i:
        return " "
    else:
        return path(i,k) + str(k) + path(k,j)

for i in range(0,3):
    for j in range(0,3):
        print "Path %s->%s: %s [%s]" % (i,j,path(i,j),p[i][j])

